#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QDirIterator qd("directory",QDir::NoDotAndDotDot);
    while(qd.hasNext())
    {
        qDebug() << qd.next();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
