#ifndef SETTINGSHANDLER_H
#define SETTINGSHANDLER_H

#include <QObject>
#include "info_header.h"


class settingsHandler : public QObject
{
    Q_OBJECT
public:
    //functions
    explicit settingsHandler(QObject *parent = 0);
    ~settingsHandler();

        //Setters
        void setWorkFolder(QString folder);
        void setOrdersFolder(QString folder);
        void setInvoicesFolder(QString folder);
        void setMenuItems(QString item);
        //Getters
        QString workFolder();
        QString ordersFolder();
        QString invoicesFolder();
        QList <QString> menuItems();
    //variables

    struct settingsStruct
    {
        QString WorkFolder;
        QString ordersFolder;
        QString invoicesFolder;
        QList <QString> menuItems;
    } settings;

signals:

public slots:
};

#endif // SETTINGSHANDLER_H
