#ifndef DIALOGABOUT_H
#define DIALOGABOUT_H

#include <QDialog>
#include "info_header.h"

namespace Ui {
class dialogAbout;
}

class dialogAbout : public QDialog
{
    Q_OBJECT

public:
    explicit dialogAbout(QWidget *parent = 0);
    ~dialogAbout();

private slots:
    void on_exit_clicked();

private:
    Ui::dialogAbout *ui;
};

#endif // DIALOGABOUT_H
