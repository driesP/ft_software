#ifndef XMLHANDLER_H
#define XMLHANDLER_H

#include <QObject>
#include "info_header.h"
#include <QXmlStreamReader>
#include <QFile>
#include "settingshandler.h"

class XMLHandler : public QObject
{
    Q_OBJECT
public:
    explicit XMLHandler(QObject *parent = 0);
    ~XMLHandler();
    void readSimple(QString file = "settings/settings.xml");
    void readMenuItems(QString file, settingsHandler *handler);
signals:

public slots:

private:
QXmlStreamReader *xmlReader;
};

#endif // XMLHANDLER_H
