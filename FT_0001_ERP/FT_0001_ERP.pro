#-------------------------------------------------
#
# Project created by QtCreator 2016-06-03T00:09:01
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FT_0001_ERP
TEMPLATE = app


SOURCES += main.cpp\
        corewindow.cpp \
    dialogabout.cpp \
    settingshandler.cpp \
    xmlhandler.cpp \
    menubutton.cpp

HEADERS  += corewindow.h \
    info_header.h \
    dialogabout.h \
    settingshandler.h \
    xmlhandler.h \
    menubutton.h

FORMS    += corewindow.ui \
    dialogabout.ui

RESOURCES += \
    resource_icons.qrc
