#ifndef COREWINDOW_H
#define COREWINDOW_H

#include <QMainWindow>
#include "info_header.h"
#include "dialogabout.h"
#include "xmlhandler.h"
#include <QLabel>
#include "menubutton.h"

namespace Ui {
class CoreWindow;
}

class CoreWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CoreWindow(QWidget *parent = 0);
    ~CoreWindow();

private slots:
    void on_actionAbout_triggered();

private:
    Ui::CoreWindow *ui;
    XMLHandler *xh;
    settingsHandler *sh;
    QList <QString> itemNames;
    QList <menuButton*> menuButtonListPrimary;
    QList <menuButton*> menuButtonListSecondary;
};

#endif // COREWINDOW_H
