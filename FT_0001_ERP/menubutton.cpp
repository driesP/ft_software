#include "menubutton.h"

menuButton::menuButton(QString name)
{
    buttonName = name;
    this->setText(name);
    QPalette color = this->palette();
    this->setAutoFillBackground(true);
    color.setColor(QPalette::Background, Qt::blue);
    color.setColor(QPalette::ButtonText, Qt::blue);
    this->setPalette(color);
    __debug << "button " << name <<" constructed";
}

menuButton::~menuButton()
{
    __debug << "button destructed";
}
