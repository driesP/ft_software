#include "settingshandler.h"

settingsHandler::settingsHandler(QObject *parent) : QObject(parent)
{
    __debug << "setting handler Created";
}

settingsHandler::~settingsHandler()
{
    __debug << "setting handler Destructed";
}
//Setters
void settingsHandler::setWorkFolder(QString folder)
{
    settings.WorkFolder = folder;
}

void settingsHandler::setOrdersFolder(QString folder)
{
    settings.ordersFolder = folder;
}

void settingsHandler::setInvoicesFolder(QString folder)
{
    settings.invoicesFolder = folder;
}
void settingsHandler::setMenuItems(QString item)
{
    settings.menuItems.append(item);
}

//Getters
QString settingsHandler::workFolder()
{
    return settings.WorkFolder;
}

QString settingsHandler::ordersFolder()
{
    return settings.ordersFolder;
}

QString settingsHandler::invoicesFolder()
{
    return settings.invoicesFolder;
}

QList <QString> settingsHandler::menuItems()
{
    return settings.menuItems;
}
