#ifndef INFO_HEADER
#define INFO_HEADER
#include <QString>
#include <QDebug>
#define __debug  qDebug() << __FILE__ << __LINE__

const QString name = "FTSolutions ERP";
const QString version = "0.0.0.1";
const QString stage = "ALPHA";
const QString author = "Dries Peeters";
const QString license = "http://www.apache.org/licenses/LICENSE-2.0";

#endif // INFO_HEADER


