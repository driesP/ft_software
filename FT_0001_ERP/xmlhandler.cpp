#include "xmlhandler.h"

XMLHandler::XMLHandler(QObject *parent) : QObject(parent)
{

    __debug << "XML handler Created";
}
XMLHandler::~XMLHandler()
{
    __debug << "XML handler Destructed";
}

void XMLHandler::readSimple(QString file)
{
    QFile *inFile = new QFile(file);
    inFile->open(QIODevice::ReadOnly| QIODevice::Text);
    xmlReader = new QXmlStreamReader(inFile);

    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
            // Read next element
            QXmlStreamReader::TokenType token = xmlReader->readNext();
            //If token is just StartDocument - go to next
            if(token == QXmlStreamReader::StartDocument) {
                    continue;
            }
            //If token is StartElement - read it
            if(token == QXmlStreamReader::StartElement) {
                    if(xmlReader->name() == "workDir") {
                        __debug << xmlReader->readElementText();
                    }
            }
    }

    if(xmlReader->hasError()) {
        __debug << "error";
    }
    xmlReader->clear();
    inFile->close();

}
void XMLHandler::readMenuItems(QString file, settingsHandler *handler)
{
    QFile *inFile = new QFile(file);
    inFile->open(QIODevice::ReadOnly| QIODevice::Text);
    xmlReader = new QXmlStreamReader(inFile);

    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
            // Read next element
            QXmlStreamReader::TokenType token = xmlReader->readNext();
            //If token is just StartDocument - go to next
            if(token == QXmlStreamReader::StartDocument) {
                    continue;
            }
            //If token is StartElement - read it
            if(token == QXmlStreamReader::StartElement) {
                    if(xmlReader->name() == "menuItem") {
                        handler->setMenuItems(xmlReader->readElementText());
                    }
            }
    }

    if(xmlReader->hasError()) {
        __debug << "error";
    }
    xmlReader->clear();
    inFile->close();
}
