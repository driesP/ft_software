#include "corewindow.h"
#include "ui_corewindow.h"



CoreWindow::CoreWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CoreWindow)
{
    ui->setupUi(this);
    QString title = name + " " + version + " " + stage;
    this->setWindowTitle(title);
    QIcon windowIcon(":/icons/glyphs/Resources/Icons/logo.png");
    this->setWindowIcon(windowIcon);
    this->showMaximized();
    xh = new XMLHandler();
    sh = new settingsHandler();
    xh->readMenuItems("settings/settings.xml", sh);
    itemNames = sh->menuItems();
    for(int i = 0; i< itemNames.length(); i++)
    {
        menuButton * tempButton = new menuButton(itemNames.at(i));
        ui->menuPrimary->addWidget(tempButton);
        menuButtonListPrimary.append(tempButton);

    }



}

CoreWindow::~CoreWindow()
{
    delete ui;
}

void CoreWindow::on_actionAbout_triggered()
{
    dialogAbout *dA = new dialogAbout();
    dA->show();
}
