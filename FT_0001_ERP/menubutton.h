#ifndef MENUBUTTON_H
#define MENUBUTTON_H

#include <QPushButton>
#include "info_header.h"

class menuButton : public QPushButton
{
public:
    menuButton(QString text);
    ~menuButton();
private:
    QString buttonName;
};

#endif // MENUBUTTON_H
