#include "dialogabout.h"
#include "ui_dialogabout.h"

dialogAbout::dialogAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogAbout)
{

    ui->setupUi(this);
    this->setWindowTitle("About");
    QIcon windowIcon(":/icons/glyphs/Resources/Icons/logo.png");
    this->setWindowIcon(windowIcon);
    ui->Author_edit->setText(author);
    ui->License_edit->setText(license);
    ui->Version_edit->setText(version);
    ui->Name_edit->setText(name);
    ui->Stage_edit->setText(stage);
}

dialogAbout::~dialogAbout()
{
    delete ui;
}

void dialogAbout::on_exit_clicked()
{
    this->close();
}
