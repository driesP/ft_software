#ifndef PRODUCT_H
#define PRODUCT_H

#include <QObject>

class product : public QObject
{
    Q_OBJECT
public:
    explicit product(int id, QString barcode, QString bExt, QString omschrijving, QString leverancier, int aantal, float prijs, QString type, QObject *parent = 0);
    ~product();
    setId(int a);
    setBarcode(QString a);
    setbExt(QString a);
    setOmschrijving(QString a);
    setLeverancier(QString a);
    setAantal(int a);
    setPrijs(float a);
    setType(QString a);
    int id();
    QString barcode();
    QString bExt();
    QString omschrijving();
    QString leverancier();
    int aantal();
    float prijs();
    QString type();

signals:

public slots:

private:
    int v_id;
    QString v_barcode;
    QString v_bExt;
    QString v_omschrijving;
    QString v_leverancier;
    int v_aantal;
    float v_prijs;
    QString v_type;


};

#endif // PRODUCT_H
