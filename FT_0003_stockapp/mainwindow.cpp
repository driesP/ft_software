#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mh = new mysqlHelper(this);
    ui->gebruikerInfo->setEnabled(true);
    ui->barcodeInput->setEnabled(false);
    ui->productInfo->setEnabled(false);
    ui->uitschrijvenInfo->setEnabled(false);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_gebruikerInput_returnPressed()
{
    QString name = mh->getUser(ui->gebruikerInput->text());
    if(name != "")
    {
        ui->gebruikerInput->setText(name);
        ui->gebruikerInput->setEnabled(false);
        ui->barcodeInput->setEnabled(true);
        ui->barcodeInput->setFocus();
    }
}

void MainWindow::on_barcodeInput_returnPressed()
{
    pro = mh->getProduct(ui->barcodeInput->text());
    if(pro->barcode() != "")
    {
        ui->barcodeText->setText(pro->barcode());
        ui->externceCodeText->setText(pro->bExt());
        ui->omschrijvingText->setText(pro->omschrijving());
        ui->leverancierText->setText(pro->leverancier());
        ui->aantalText->setText(QString::number(pro->aantal()));
        ui->prijsText->setText(QString::number(pro->prijs()));
        ui->typeText->setText(pro->type());

        ui->barcodeInput->setEnabled(false);
        ui->productInfo->setEnabled(true);
        ui->uitschrijvenInfo->setEnabled(true);
        ui->aantalVariable->setFocus();
    }
}
