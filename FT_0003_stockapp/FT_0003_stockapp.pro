#-------------------------------------------------
#
# Project created by QtCreator 2016-06-21T08:06:20
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FT_0003_stockapp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mysqlhelper.cpp \
    product.cpp

HEADERS  += mainwindow.h \
    mysqlhelper.h \
    debug.h \
    product.h

FORMS    += mainwindow.ui
