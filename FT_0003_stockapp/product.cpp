#include "product.h"

product::product(int id, QString barcode, QString bExt, QString omschrijving, QString leverancier, int aantal, float prijs, QString type,QObject *parent) : QObject(parent)
{
    v_id = id;
    v_barcode = barcode;
    v_bExt = bExt;
    v_omschrijving = omschrijving;
    v_leverancier = leverancier;
    v_aantal = aantal;
    v_prijs = prijs;
    v_type = type;
}

product::~product()
{

}
product::setId(int a)
{
    v_id = a;
}
product::setBarcode(QString a)
{
    v_barcode = a;
}
product::setbExt(QString a)
{
    v_bExt = a;
}
product::setOmschrijving(QString a)
{
    v_omschrijving = a;
}
product::setLeverancier(QString a)
{
    v_leverancier = a;
}
product::setAantal(int a)
{
    v_aantal  = a;
}
product::setPrijs(float a)
{
    v_prijs = a;
}
product::setType(QString a)
{
    v_type = a;
}
int product::id()
{
    return v_id;
}
QString product::barcode()
{
    return v_barcode;
}
QString product::bExt()
{
    return v_bExt;
}
QString product::omschrijving()
{
    return v_omschrijving;
}
QString product::leverancier()
{
    return v_leverancier;
}
int product::aantal()
{
    return v_aantal;
}
float product::prijs()
{
    return v_prijs;
}
QString product::type()
{
    return v_type;
}
