#ifndef MYSQLHELPER_H
#define MYSQLHELPER_H

#include <QObject>
#include "debug.h"
#include <QSql>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlError>
#include <product.h>

#define server "192.168.2.96"
#define database "db_intern_deploy"
#define username "stock_user"
#define password "FT2015solutions"

class mysqlHelper : public QObject
{
    Q_OBJECT
public:
    explicit mysqlHelper(QObject *parent = 0);
    ~mysqlHelper();
    QString getUser(QString user);
    product *getProduct(QString barcode);

signals:

public slots:


private:
    QSqlDatabase db;
    product *pro;
};

#endif // MYSQLHELPER_H
