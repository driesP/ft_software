#include "mysqlhelper.h"

mysqlHelper::mysqlHelper(QObject *parent) : QObject(parent)
{
    db = QSqlDatabase::addDatabase("QMYSQL3");

    db.setHostName(server);
    db.setDatabaseName(database);
    db.setUserName(username);
    db.setPassword(password);
    db.open();
    if(!(db.open()))
    {
        __debug << db.lastError();
        __debug << "error on opening application!";
    }
}
mysqlHelper::~mysqlHelper()
{
    db.close();
    __debug << "destructed mysql connection";
}
QString mysqlHelper::getUser(QString user)
{
    QString name;
    QSqlQuery query;
    query.exec("SELECT u_name from tbl_user_protected WHERE userCode_bar ="+user);
    while(query.next())
    {
        name = query.value(0).toString();
    }
    return name;
}

product * mysqlHelper::getProduct(QString barcode)
{

    QSqlQuery query;
    query.exec("SELECT id,p_id,p_idExt, p_omsch,l.l_naam,p_aantal,p_prijs,p_type from tbl_parts LEFT JOIN tbl_leveranciers as l on p_Lid = l_id WHERE p_id ="+barcode);
    while(query.next())
    {
        pro =  new product(query.value(0).toInt(),query.value(1).toString(),query.value(2).toString(),query.value(3).toString(),query.value(4).toString(),query.value(5).toInt(),query.value(6).toFloat(),query.value(7).toString(),this);
    }
    return pro;
}
