#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <mysqlhelper.h>
#include <product.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_gebruikerInput_returnPressed();

    void on_barcodeInput_returnPressed();

private:
    Ui::MainWindow *ui;
    mysqlHelper *mh;
    product *pro;
};

#endif // MAINWINDOW_H
