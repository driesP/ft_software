/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *barcodeInput;
    QSpacerItem *horizontalSpacer;
    QGroupBox *productInfo;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_5;
    QLabel *barcodeText;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_6;
    QLabel *externceCodeText;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_7;
    QLabel *omschrijvingText;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_8;
    QLabel *leverancierText;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_9;
    QLabel *aantalText;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_10;
    QLabel *prijsText;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_19;
    QLabel *typeText;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QGroupBox *gebruikerInfo;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QLineEdit *gebruikerInput;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label;
    QLabel *datumText;
    QPushButton *uitloggen;
    QSpacerItem *verticalSpacer;
    QGroupBox *uitschrijvenInfo;
    QVBoxLayout *verticalLayout_5;
    QSpinBox *aantalVariable;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *inschrijven;
    QPushButton *uitschrijven;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(897, 611);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_3 = new QHBoxLayout(centralWidget);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        barcodeInput = new QLineEdit(centralWidget);
        barcodeInput->setObjectName(QStringLiteral("barcodeInput"));

        horizontalLayout_2->addWidget(barcodeInput);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout_2);

        productInfo = new QGroupBox(centralWidget);
        productInfo->setObjectName(QStringLiteral("productInfo"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(productInfo->sizePolicy().hasHeightForWidth());
        productInfo->setSizePolicy(sizePolicy);
        productInfo->setAlignment(Qt::AlignCenter);
        verticalLayout_4 = new QVBoxLayout(productInfo);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_5 = new QLabel(productInfo);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_7->addWidget(label_5);

        barcodeText = new QLabel(productInfo);
        barcodeText->setObjectName(QStringLiteral("barcodeText"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(barcodeText->sizePolicy().hasHeightForWidth());
        barcodeText->setSizePolicy(sizePolicy1);

        horizontalLayout_7->addWidget(barcodeText);


        verticalLayout_4->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_6 = new QLabel(productInfo);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_8->addWidget(label_6);

        externceCodeText = new QLabel(productInfo);
        externceCodeText->setObjectName(QStringLiteral("externceCodeText"));
        sizePolicy1.setHeightForWidth(externceCodeText->sizePolicy().hasHeightForWidth());
        externceCodeText->setSizePolicy(sizePolicy1);

        horizontalLayout_8->addWidget(externceCodeText);


        verticalLayout_4->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_7 = new QLabel(productInfo);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout_9->addWidget(label_7);

        omschrijvingText = new QLabel(productInfo);
        omschrijvingText->setObjectName(QStringLiteral("omschrijvingText"));
        sizePolicy1.setHeightForWidth(omschrijvingText->sizePolicy().hasHeightForWidth());
        omschrijvingText->setSizePolicy(sizePolicy1);

        horizontalLayout_9->addWidget(omschrijvingText);


        verticalLayout_4->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        label_8 = new QLabel(productInfo);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_10->addWidget(label_8);

        leverancierText = new QLabel(productInfo);
        leverancierText->setObjectName(QStringLiteral("leverancierText"));
        sizePolicy1.setHeightForWidth(leverancierText->sizePolicy().hasHeightForWidth());
        leverancierText->setSizePolicy(sizePolicy1);

        horizontalLayout_10->addWidget(leverancierText);


        verticalLayout_4->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label_9 = new QLabel(productInfo);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_11->addWidget(label_9);

        aantalText = new QLabel(productInfo);
        aantalText->setObjectName(QStringLiteral("aantalText"));
        sizePolicy1.setHeightForWidth(aantalText->sizePolicy().hasHeightForWidth());
        aantalText->setSizePolicy(sizePolicy1);

        horizontalLayout_11->addWidget(aantalText);


        verticalLayout_4->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        label_10 = new QLabel(productInfo);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_12->addWidget(label_10);

        prijsText = new QLabel(productInfo);
        prijsText->setObjectName(QStringLiteral("prijsText"));
        sizePolicy1.setHeightForWidth(prijsText->sizePolicy().hasHeightForWidth());
        prijsText->setSizePolicy(sizePolicy1);

        horizontalLayout_12->addWidget(prijsText);


        verticalLayout_4->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        label_19 = new QLabel(productInfo);
        label_19->setObjectName(QStringLiteral("label_19"));

        horizontalLayout_13->addWidget(label_19);

        typeText = new QLabel(productInfo);
        typeText->setObjectName(QStringLiteral("typeText"));
        sizePolicy1.setHeightForWidth(typeText->sizePolicy().hasHeightForWidth());
        typeText->setSizePolicy(sizePolicy1);

        horizontalLayout_13->addWidget(typeText);


        verticalLayout_4->addLayout(horizontalLayout_13);


        verticalLayout_2->addWidget(productInfo);


        horizontalLayout_3->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        gebruikerInfo = new QGroupBox(centralWidget);
        gebruikerInfo->setObjectName(QStringLiteral("gebruikerInfo"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(gebruikerInfo->sizePolicy().hasHeightForWidth());
        gebruikerInfo->setSizePolicy(sizePolicy2);
        gebruikerInfo->setAlignment(Qt::AlignCenter);
        verticalLayout_3 = new QVBoxLayout(gebruikerInfo);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_2 = new QLabel(gebruikerInfo);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_4->addWidget(label_2);

        gebruikerInput = new QLineEdit(gebruikerInfo);
        gebruikerInput->setObjectName(QStringLiteral("gebruikerInput"));

        horizontalLayout_4->addWidget(gebruikerInput);


        verticalLayout_3->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label = new QLabel(gebruikerInfo);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_5->addWidget(label);

        datumText = new QLabel(gebruikerInfo);
        datumText->setObjectName(QStringLiteral("datumText"));

        horizontalLayout_5->addWidget(datumText);


        verticalLayout_3->addLayout(horizontalLayout_5);

        uitloggen = new QPushButton(gebruikerInfo);
        uitloggen->setObjectName(QStringLiteral("uitloggen"));

        verticalLayout_3->addWidget(uitloggen);


        horizontalLayout->addWidget(gebruikerInfo);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        uitschrijvenInfo = new QGroupBox(centralWidget);
        uitschrijvenInfo->setObjectName(QStringLiteral("uitschrijvenInfo"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(uitschrijvenInfo->sizePolicy().hasHeightForWidth());
        uitschrijvenInfo->setSizePolicy(sizePolicy3);
        uitschrijvenInfo->setAlignment(Qt::AlignCenter);
        verticalLayout_5 = new QVBoxLayout(uitschrijvenInfo);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        aantalVariable = new QSpinBox(uitschrijvenInfo);
        aantalVariable->setObjectName(QStringLiteral("aantalVariable"));

        verticalLayout_5->addWidget(aantalVariable);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        inschrijven = new QPushButton(uitschrijvenInfo);
        inschrijven->setObjectName(QStringLiteral("inschrijven"));

        horizontalLayout_6->addWidget(inschrijven);

        uitschrijven = new QPushButton(uitschrijvenInfo);
        uitschrijven->setObjectName(QStringLiteral("uitschrijven"));

        horizontalLayout_6->addWidget(uitschrijven);


        verticalLayout_5->addLayout(horizontalLayout_6);


        verticalLayout->addWidget(uitschrijvenInfo);


        horizontalLayout_3->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        productInfo->setTitle(QApplication::translate("MainWindow", "product info", 0));
        label_5->setText(QApplication::translate("MainWindow", "Barcode:", 0));
        barcodeText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_6->setText(QApplication::translate("MainWindow", "externe code:", 0));
        externceCodeText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_7->setText(QApplication::translate("MainWindow", "omschrijving:", 0));
        omschrijvingText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_8->setText(QApplication::translate("MainWindow", "leverancier:", 0));
        leverancierText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_9->setText(QApplication::translate("MainWindow", "aantal:", 0));
        aantalText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_10->setText(QApplication::translate("MainWindow", "prijs:", 0));
        prijsText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_19->setText(QApplication::translate("MainWindow", "type:", 0));
        typeText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        gebruikerInfo->setTitle(QApplication::translate("MainWindow", "gebruiker", 0));
        label_2->setText(QApplication::translate("MainWindow", "gebruiker", 0));
        label->setText(QApplication::translate("MainWindow", "datum", 0));
        datumText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        uitloggen->setText(QApplication::translate("MainWindow", "uitloggen", 0));
        uitschrijvenInfo->setTitle(QApplication::translate("MainWindow", "uitschrijven", 0));
        inschrijven->setText(QApplication::translate("MainWindow", "inschrijven", 0));
        uitschrijven->setText(QApplication::translate("MainWindow", "uitschrijven", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
